# lit

> Nobody:<br>
> Me: This editor is lit, fam.

A tiny text editor. Neat!

<https://viewsourcecode.org/snaptoken/kilo/>

## Usage

Open a file by specifying it on the command-line: `lit <filename>`.

Move the cursor with the arrow keys. Save changes with `Ctrl+S`.
Quit by pressing `Ctrl+Q`.

## Building from source

Requires a C compiler and one of the following build systems.

This program probably won't work on Windows without significant reworking,
since it relies on POSIX terminal features that have no direct equivalent on
Windows. It will build and run in Cygwin and Windows Subsystem for Linux, but
don't count on MSVC or MinGW support for now.

### Meson and Ninja

```bash
meson setup build
ninja -C build
./build/src/lit
```

### CMake and GNU make

```bash
cmake -B build
cmake --build build
./build/bin/lit
```

## Future plans

- [ ] port to ncurses or a similar library
- [ ] Windows console support

## License

This code is made available under the same terms as [Kilo](https://github.com/antirez/kilo),
the program it's based upon. For more details, see [LICENSE](LICENSE).

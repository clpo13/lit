#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

/**
 * Clears the screen by writing the VT100 Erase In Display
 * command to the terminal. After clearing, the cursor is
 * positioned at the top-left corner with Cursor Position.
 */
void clearScreen() {
    write(STDOUT_FILENO, "\x1b[2J", 4);
    write(STDOUT_FILENO, "\x1b[H", 3);
}

/**
 * Determines the coordinates of the cursor.
 *
 * Part of a fallback mechanism to determine the size of the
 * current terminal window.
 */
int getCursorPosition(int *rows, int *cols) {
    char buf[32];
    unsigned int i = 0;

    if (write(STDOUT_FILENO, "\x1b[6n", 4) != 4) {
        return -1;
    }

    while (i < sizeof(buf) - 1) {
        if (read(STDIN_FILENO, &buf[i], 1) != 1) break;
        if (buf[i] == 'R') break;
        i++;
    }
    buf[i] = '\0';

    if (buf[0] != '\x1b' || buf[1] != '[') {
        return -1;
    }
    if (sscanf(&buf[2], "%d;%d", rows, cols) != 2) {
        return -1;
    }

    return 0;
}

/**
 * Determines the size of the current window.
 *
 * Tries to get the info from ioctl first. If that doesn't work, it
 * falls back to placing the cursor as far as possible to the bottom-right
 * and reading those coordinates.
 *
 * @param rows number of rows
 * @param cols number of columns
 * @returns 0 if the window properties could successfully
 *          be determined or -1 if not
 */
int getWindowSize(int *rows, int *cols) {
    struct winsize ws;

    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws) == -1 || ws.ws_col == 0) {
        if (write(STDOUT_FILENO, "\x1b[999C\x1b[999B", 12) != 12) {
            return -1;
        }
        return getCursorPosition(rows, cols);
    } else {
        *cols = ws.ws_col;
        *rows = ws.ws_row;
        return 0;
    }
}

#ifndef RAWMODE_H
#define RAWMODE_H

void disableRawMode();
void enableRawMode();

#endif /* RAWMODE_H */

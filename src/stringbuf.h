#ifndef STRINGBUF_H
#define STRINGBUF_H

struct abuf {
    char *b;
    int len;
};

void abAppend(struct abuf *, const char *, int len);
void abFree(struct abuf *);

#define ABUF_INIT {NULL, 0}

#endif

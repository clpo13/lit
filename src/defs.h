#ifndef DEFS_H
#define DEFS_H

#define CTRL_KEY(k) ((k) & 0x1f)
#define TAB_STOP 8
#define QUIT_CONFIRM 3

#endif /* DEFS_H */

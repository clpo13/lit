#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "util.h"

/**
 * Prints an error message and exits the program.
 *
 * @param s Error message to print
 */
void die(const char *s) {
    clearScreen();
    perror(s);
    exit(1);
}

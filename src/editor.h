#ifndef EDITOR_H
#define EDITOR_H

#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#define _GNU_SOURCE

#include <termios.h>
#include <time.h>

#include "erow.h"

/**
 * Stores editor and terminal configuration data.
 */
struct editorConfig {
    int cx, cy;
    int rx;
    int rowoff;
    int coloff;
    int screenrows;
    int screencols;
    int numrows;
    erow *row;
    int dirty;
    char *filename;
    char statusmsg[80];
    time_t statusmsg_time;
    struct termios orig_termios;
};

void editorOpen(char *);
void editorProcessKeypress();
void editorRefreshScreen();
void editorSetStatusMessage(const char *, ...);
void initEditor();

#endif /* EDITOR_H */
